ARG DOCKER_HUB_URL
FROM ${DOCKER_HUB_URL}alpine:edge
# FROM alpine:3.16
# The problem is not really with Alpine 3.17, the problem is with curl 7.86.
# Once curl 7.87 is released Wednesday 21 December 2022, this will no longer be a problem.

ARG FIX_ALL_GOTCHAS_SCRIPT_LOCATION
ARG ETC_ENVIRONMENT_LOCATION
ARG CLEANUP_SCRIPT_LOCATION

# Depending on the base image used, we might lack wget/curl/etc to fetch ETC_ENVIRONMENT_LOCATION.
ADD $FIX_ALL_GOTCHAS_SCRIPT_LOCATION .
ADD $CLEANUP_SCRIPT_LOCATION .

RUN set -o allexport \
    && . ./fix_all_gotchas.sh \
    && set +o allexport \
    && apk add --no-cache tar jq curl \
    && . ./cleanup.sh
